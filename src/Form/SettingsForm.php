<?php declare(strict_types = 1);

namespace Drupal\search_api_extras\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Search API Extras settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'search_api_extras_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['search_api_extras.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['override_terms_parser'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Override default Multiple Terms parser to add the searched terms plus the original phrase.'),
      '#description' => $this->t('This will result in documents that contain the exact search phrase to rank more highly than those that merely contain the search individual terms (that is, documents containing "lunch menu" exactly will rank more highly than those that only talk about "lunch" and "menu" separately). Note: Please clear the Drupal cache after changing this setting for it to take effect.'),
      '#default_value' => $this->config('search_api_extras.settings')->get('override_terms_parser'),
    ];
    return parent::buildForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('search_api_extras.settings')
      ->set('override_terms_parser', $form_state->getValue('override_terms_parser'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
