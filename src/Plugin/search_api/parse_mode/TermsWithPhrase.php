<?php

namespace Drupal\search_api_extras\Plugin\search_api\parse_mode;

use Drupal\search_api\Annotation\SearchApiParseMode;
use Drupal\search_api\Plugin\search_api\parse_mode\Terms;

/**
 * Represents a parse mode that parses the sentence into a sloppy search.
 *
 * @SearchApiParseMode(
 *   id = "terms_with_phrase",
 *   label = @Translation("FCPS: Multiple words plus phrase"),
 *   description = @Translation("Behaves like the multiple word parse except it puts all the separate terms together as an additional phrase on the query. This lets us match all docs containing any of the words, but then boosts docs that contain the exact phrase."),
 * )
 */
class TermsWithPhrase extends Terms {
  /**
   * {@inheritdoc}
   */
  public function parseInput($keys) {
    $ret = parent::parseInput($keys);
    if ($this->conjunction == 'OR') {
      $ret[] = implode(' ', array_filter($ret, 'is_numeric', ARRAY_FILTER_USE_KEY));
    }
    return $ret;
  }
}
