<?php

namespace Drupal\search_api_extras\Plugin\views\sort;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Annotation\ViewsSort;
use Drupal\views\Plugin\views\sort\SortPluginBase;

/**
 * Provides a conditional sort plugin for Search API views.
 *
 * @ViewsSort("search_api_extras")
 */
class SearchApiExtrasSort extends SortPluginBase {

  /**
   * The associated views query object.
   *
   * @var \Drupal\search_api\Plugin\views\query\SearchApiQuery
   */
  public $query;

  /**
   * {@inheritdoc}
   */
  public function query() {
    // When there are exposed sorts, the "exposed form" plugin will set
    // $query->orderby to an empty array. Therefore, if that property is set,
    // we here remove all previous sorts.
    // @todo Is this still true in D8?
    // @todo Check whether #2145547 is still a problem here.
    if (isset($this->query->orderby)) {
      unset($this->query->orderby);
      $sort = &$this->query->getSort();
      $sort = [];
    }

    $conditional_fields = $this->options['ignore_when_exposed_filters_empty'];

    // Always add the sort if there are no conditional fields specified.
    $add_sort = empty($conditional_fields);

    $exposed_input = $this->view->getExposedInput();
    foreach ($conditional_fields as $conditional_field) {
      if (!empty($exposed_input[$conditional_field])) {
        $add_sort = true;
        break;
      }
    }
    if ($add_sort) $this->query->sort($this->realField, $this->options['order']);
  }

  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['ignore_when_exposed_filters_empty'] = ['default' => []];
    return $options;
  }


  /**
   * Basic options for all sort criteria.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $options = [];
    $filters = $this->displayHandler->getOption('filters');
    $filters = array_filter($filters, function ($value) {
      return $value['exposed'] && $value['plugin_id'] == 'search_api_fulltext';
    });
    foreach ($filters as $filter) {
      $options[$filter['expose']['identifier']] = $filter['expose']['label'] ?: $filter['expose']['identifier'];
    }

    if (!empty($options)) {
      $form['ignore_start'] = ['#value' => '<div class="clearfix">'];
      $form['ignore_when_exposed_filters_empty'] = [
        '#title' => $this->t('Ignore if empty', [], ['context' => 'Search sort options']),
        '#description' => $this->t('If any of the exposed fields you select contains user input then this relevance sort will be applied. Otherwise it will not be applied and only your other selected sorts will take effect.'),
        '#type' => 'select',
        '#options' => $options,
        '#multiple' => TRUE,
        '#default_value' => $this->options['ignore_when_exposed_filters_empty']
      ];
      $form['ignore_end'] = ['#value' => '</div>'];
    }
  }
}
